module.exports = async function (browser, {inputParameters}) {
  if (!inputParameters.testTargetUrl) {
      throw 'Invalid event data!';
    }
  
  const page = await browser.newPage();
  await page.setViewport({ width: 1366, height: 768});
  await page.goto(inputParameters.testTargetUrl);
  // await page.screenshot({path: process.env.SCREENSHOTS_PATH + '/' + 'screenshot.png'});
  const pageTitle = await page.title();
  await page.click(inputParameters.clickId);
  await page.waitFor(4000);
  // await page.screenshot({path: process.env.SCREENSHOTS_PATH + '/' + 'screenshot_2.png'});
  await browser.close();
  return { tytul: pageTitle, targetNew: inputParameters.testTargetUrl, clicked: inputParameters.clickId };
}
