module.exports = async function (browser, inputParameters) {
    if (!inputParameters.testTargetUrl || !inputParameters.taskTitle) {
      throw 'Invalid event data!';
    }
  
    // const page = await browser.newPage();
  
    // await page.goto(inputParameters.testTargetUrl);
  
    // await page.click('.add-task__text');
    // await (await page.$('.add-task__content')).type(inputParameters.taskTitle);
    // await page.click('.add-task__submit');
    // await page.waitFor(() => !document.querySelector('.add-task__content'));
  
    // return { created: 'ok', taskTitle: inputParameters.taskTitle };

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 768});
    await page.goto(inputParameters.testTargetUrl);
    const pageTitle = await page.title();
    await browser.close();
  };

  return { tytul: pageTitle }
  