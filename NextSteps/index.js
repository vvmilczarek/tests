module.exports = async function (browser, {inputParameters, previousResult}) {
    
    const page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 768});
    await page.goto(previousResult.targetNew);
    await page.click(inputParameters.clickId);
    const pageTitle = await page.title();
    await page.waitFor(4000);
    await page.screenshot({path: process.env.SCREENSHOTS_PATH + '/' + 'screenshot.png'});
    const urlNextPage = await page.url();
    await browser.close();
    return { tytul: pageTitle, targetNew: urlNextPage, clicked: inputParameters.clickId };
}